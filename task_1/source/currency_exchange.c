#include <stdio.h>
#include <stdbool.h>
#include <time.h>
#include <stdlib.h>

//A SEK to EUR converter since the assingment was ambigous and it seemed like the currency and it's exchange rate should be hard coded at some points in the lab3.pdf text and at other points not.

float exchange(float sek, float rate);

int main(int argc, char** argv)
{
    float sek;
    int samples;    //added
    int i;          //added
    const float rate = 10.23;   //As far as i understand the task this is supposed to be hard coded in the main fucntion
    //as long as the argument is sent to the function and not actually hard coded within the function.
    srand(time(NULL));  //added

    printf("How many samples do you want?: ");  //changed
    scanf("%d", &samples);      //changed

    for (i=0; i<samples; i++)   //added
    {
        sek = rand()%1000+1;    //added
        printf("%.2f SEK is equal to %.2f EUR\n", sek, exchange(sek, rate));    //changed
    }

    return 0;
}


float exchange(float sek, float rate)
{
    return sek/rate;
}
