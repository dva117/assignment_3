#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
int guessing();

//a guessing game program.


int main(int argc, char** argv)
{
    int run_main = 1;
    int valid_input = 1;
    int valid_run_input = 1;
    int counter;
    while(run_main)
    {   
        counter = 0;
        printf("\nI'm thinking of a number between 1 and 100, guess which!\n");

        if(valid_input > 0)
        {
            counter = guessing();
        }

        printf("\nYour total amount of valid guesses was %d.", counter);
        while (true)
        {
            printf("\n\nShall we play again?(1 for yes, 0 for no): ");
            valid_input = scanf("%d", &valid_run_input);
            fflush(stdin);      //so non integers count as invalid input.
            if (valid_run_input == 1 || valid_run_input == 0)
            {
                run_main = valid_run_input;
                break;
            }
            else
            {
                printf("\nInvalid input! Try again.");
            }
        }
    }
}

int guessing()
{ 
    int guess = 0;
    int counter = 0;
    srand(time(NULL));
    int random_number = rand()%100+1;

    while (true)
    {
        while (true)
        {
            printf("\nGuess: ");
            if (scanf("%d", &guess) > 0)
            {
                break;
            }
            else
            {
                printf("Invalid input! Try again.");
            }
            fflush(stdin);       //so non integers count as invalid input.
        }
        counter ++;

        if (guess < 1 || guess > 100)
        {
            printf("Invalid input! Try again.");

            if (counter > 0)
            {
                counter = counter -1;
            }
            else
            {
                counter = 0;
            }
        }
        else if (guess == random_number)
        {
            printf("Congratulations, that is correct!");
            break;
        }
        else if (guess > random_number)
        {
            printf("Your guess is too high, try again!");
        }
        else if (guess < random_number)
        {
            printf("Your guess is too low, try again!");
        }
        fflush(stdin);      //so non integers count as invalid input.
    }
    return counter;
}
